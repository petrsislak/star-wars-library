import React, {useState, useEffect} from "react";
import useFetch from "../hooks/useFetch";
import Character from '../components/Character';
import Loader from "../components/Loader";
 
const Characters = () => {
        const [characters, setCharacters] = useState([]);
        const {get, post, loading} = useFetch('https://swapi.dev/api/');
    
        useEffect(() => {
            get('people')
            .then(data => {
                setCharacters(data.results);
            })
        });
    
        return (<div className="main-content">
                <h2>
                    Star Wars characters:
                </h2>
                <div className="characters-header">
                    <div className="char-header-element">Name</div>
                    <div className="char-header-element">Height</div>
                    <div className="char-header-element">I/O</div>
                    <div className="char-header-element">Homeworld</div>
                </div>
                <div className="horizontal-line"></div>
                <div className="characters-container">
                    {loading ? <Loader /> : ''}
                    {characters.map(character => <Character key={character.name} name={character.name} height={character.height} gender={character.gender} world={character.homeworld}></Character>)}
                </div>
            </div>)
    };
 
export default Characters;