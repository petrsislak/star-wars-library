import React, { useState, useEffect } from "react";
import useFetch from "../hooks/useFetch";
import Movie from "../components/Movie";
import Loader from "../components/Loader";

const Movies = () => {
  const [movies, setMovies] = useState([]);
  const { get, post, loading } = useFetch('https://swapi.dev/api/');

  useEffect(() => {
    get('films').then((data) => {
      setMovies(data.results);
    });
  }, []);

  const handleChronologicalOrder = () => {
    const sortedMovies = [...movies].sort((a, b) => a.episode_id - b.episode_id);
    setMovies(sortedMovies);
  };

  const handleReleaseDateOrder = () => {
    const sortedMovies = [...movies].sort((a, b) => {
      const dateA = new Date(a.release_date);
      const dateB = new Date(b.release_date);
      return dateA - dateB;
    });
    setMovies(sortedMovies);
  };

  const handleShowText = () => {
    const text = 'Pew Pew';

    const getRandomPosition = () => {
      const innerWidthMargin = window.innerWidth * 0.05;
      const innerHeightMargin = window.innerHeight * 0.05;
      const randomX = Math.floor(Math.random() * (window.innerWidth - 2 * innerWidthMargin)) + innerWidthMargin;
      const randomY = Math.floor(Math.random() * (window.innerHeight - 2 * innerHeightMargin)) + innerHeightMargin;
      return { x: randomX, y: randomY };
    };

    const randomPosition = getRandomPosition();

    const textElement = document.createElement('div');
    textElement.innerText = text;
    textElement.style.position = 'fixed';
    textElement.style.left = `${randomPosition.x}px`;
    textElement.style.top = `${randomPosition.y}px`;
    textElement.style.color = 'gold';
    textElement.style.fontSize = '18px';

    document.body.appendChild(textElement);

    setTimeout(() => {
      document.body.removeChild(textElement);
    }, 3000);
  };

  return (
    <div className="main-content">
      <h2>Star Wars movies:</h2>
      <div className="movies-header">
        <button className="movies-header-element" onClick={handleChronologicalOrder} id="first">
          Chronological order
        </button>
        <button className="movies-header-element" onClick={handleReleaseDateOrder}>
          Release Date order
        </button>
        <button className="movies-header-element" id="last" onClick={handleShowText}>
          First Order!
        </button>
      </div>
      <div className="horizontal-line"></div>
      <div className="movies-container">
        {loading ? <Loader /> : ""}
        {movies.map((movie) => (
          <Movie key={movie.title} title={movie.title} date={movie.release_date}>
            {movie.title}
          </Movie>
        ))}
      </div>
    </div>
  );
};

export default Movies;
