export default function Loader() {
    return <div className="loader-container">
        <div className="stars">
            <div className="ship"></div>
        </div>
    </div>
}