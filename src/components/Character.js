import React, {useState, useEffect} from "react";

export default function Character(props) {
    const [homeWorld, setHomeWorld] = useState('');

    useEffect(() => {
        fetch(props.world).then(response => response.json()).then(data => {
            if(data){
                setHomeWorld(data.name);
            }
        })
    });

    return <div className="character">
            <div className="character-element">{props.name}</div>
            <div className="character-element">{props.height}</div>
            <div className="character-element">{props.gender}</div>
            <div className="character-element">{homeWorld ? homeWorld : 'loading...'}</div>
        </div>
}