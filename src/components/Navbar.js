import React, {useState, useEffect} from "react";
import { BrowserRouter, Route, Link, useLocation } from "react-router-dom";

function Navbar() {
  const location = useLocation();
  const [activeItem, setActiveItem] = useState('');

  useEffect(() => {
    const currentPath = location.pathname;
    setActiveItem(currentPath);
  }, [location.pathname]);

  const handleItemClick = (itemName) => {
    setActiveItem(itemName);
  };

  const generateMenuItem = (itemName) => {
    const isActive = activeItem === itemName.toLowerCase();
    const className = isActive ? 'active' : 'passive';

    return (
      <div className={`nav-bar ${itemName.substring(1).toLowerCase()} ${className}`}>
        <Link className="nav-bar-link" to={itemName.toLowerCase()} style={{ textDecoration: 'none', color: 'antiquewhite' }} onClick={() => handleItemClick(itemName)} >{itemName.substring(1)}</Link>
      </div>
    );
  };

  return (
    <nav>
      {generateMenuItem('/Movies')}
      {generateMenuItem('/Characters')}
    </nav>
  );
}

export default Navbar;