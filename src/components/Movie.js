import React, {useState} from "react";
import Loader from "./Loader";

export default function Movie(props) {
    const imageName = props.children.toLowerCase().replace(/\s/g, "-");
    const path = `http://localhost:3000/${imageName}.jpg`;
    const year = props.date.substring(0, 4);
  
    return (
      <div className="movie-card">
        <div className="background" style={{ backgroundImage: `url(${path})` }}>
            <div className="movie-info">
                <div className="movie-title">
                    {props.children} ({year})
                </div>
            </div>
        </div>
      </div>
    );
  }
//