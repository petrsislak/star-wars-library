import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import Layout from "./pages/Layout";
import Movies from "./pages/Movies";
import Characters from "./pages/Characters";
import NoPage from "./pages/NoPage";
import './App.css';

export default function App() {
  return (
    <div className="base-panel">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Navigate to="/movies" replace />} />
            <Route path="movies" element={<Movies />} />
            <Route path="characters" element={<Characters />} />
            <Route path="*" element={<NoPage />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
};